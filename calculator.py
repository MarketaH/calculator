from flask import Flask, render_template, request


app = Flask(__name__)

def add(numAa, numB):
    return int(numA) + int(numB)

@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == "GET":
        return render_template("index.html")

    numA = request.form["numA"]
    numB = request.form["numB"]

    try:
        result = add(numA, numB)
    except ValueError:
        return render_template("index.html", error="Neplatný vstup.")
    else:
        return render_template("index.html", result=result)


if __name__ == "__main__":
    app.run("0.0.0.0")
